# Game API #

A small asp.net core project for having an online leaderboard for use in games. 
Current Features are just an endpoint for retrieving top scores and and end point for adding new ones.

I set this up just to have an easy way to add online leaderboards for games made in game jams, maybe its useful to others for a similar situation.

N.B This api does not use any kind of authentication or other methods to try and prevent malicious users directly posting their score, so is only useful for games where the scoreboards are purely for fun.

#Setup
Need to run the sql/createtables.sql on some kind of sql database that your web server has access to and then amend the connection string in appsettings.json
There are power shell scripts to try each of these end points in TestScripts, you just need to amend them to set the website where its running and the APIKey for your scoreboard

