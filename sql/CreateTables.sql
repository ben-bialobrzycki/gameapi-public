﻿select * FROM SCoreBoard

Create Table ScoreBoard
(
	Id int Identity(1,1) Primary key,
	ScoreBoardKey varchar(255),
	ScoreBoardName varchar(255)
)

create table Score
(
	Id int identity(1,1) primary key,
	ScoreBoardId int references ScoreBoard(Id),
	ScorePoints int,
	ScorePlayerName varchar(255),
	CreateDate datetime,
)
GO


alter table scoreboard add AllowGuest bit null

update ScoreBoard set allowguest = 1


alter table scoreboard alter column AllowGuest bit not null
Insert into ScoreBoard (ScoreBoardKey, ScoreBoardName, AllowGuest)
Values (NEWID(), 'TestBoard', 1)

INsert Into Score (ScoreBoardId, ScorePoints, ScorePlayerName, CreateDate)
Values (1,20000,'testplayer1', GETDATE())


INsert Into Score (ScoreBoardId, ScorePoints, ScorePlayerName, CreateDate)
Values (1,50000,'testplayer2', GETDATE())
