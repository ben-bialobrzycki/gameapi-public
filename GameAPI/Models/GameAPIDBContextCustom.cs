﻿using GameAPI.ModelsDTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;

namespace GameAPI.Models
{
    public partial class GameAPIDBContext : DbContext
    {
        int DefaultScoreCount = 20;

        public GameAPIDBContext(DbContextOptions options) : base(options) { }

        internal bool TryAddScore(ScoreDTO score)
        {
            var scoreBoardQuery = from sb in ScoreBoard
                                  where sb.ScoreBoardKey == score.ScoreBoardAPIKey
                                  select sb;
            ScoreBoard scoreBoard = scoreBoardQuery.FirstOrDefault();
            if (scoreBoard == null)
                return false;
            Score scoreEntity = new Models.Score();
            Score.Attach(scoreEntity);
            scoreEntity.ScoreBoardId = scoreBoard.Id;
            scoreEntity.ScoreBoard = scoreBoard;
            scoreEntity.ScorePlayerName = score.ScorePlayerName;
            scoreEntity.ScorePoints = score.ScorePoints;
            scoreEntity.CreateDate = DateTime.Now.ToUniversalTime();
            SaveChanges();
            score.Id = scoreEntity.Id;
            return true;
        }
        
        public ScoreBoardDTO GetScoreBoardWithTopScores(string scoreBoardKey)
        {
            var scoreBoardQuery = from sb in ScoreBoard
                                  where sb.ScoreBoardKey == scoreBoardKey
                                  orderby sb.Id
                                  select new ScoreBoardDTO() { ScoreBoardId=sb.Id, ScoreBoardName = sb.ScoreBoardName };
            ScoreBoardDTO scoreBoard = scoreBoardQuery.FirstOrDefault();

            if (scoreBoard == null)
                return null;
            var scoreQuery = from s in Score
                             where s.ScoreBoardId == scoreBoard.ScoreBoardId
                             orderby s.ScorePoints descending
                             select new ScoreDTO() { ScorePoints = s.ScorePoints ?? 0, ScorePlayerName = s.ScorePlayerName, CreateDate = s.CreateDate ?? DateTime.MinValue };
            scoreBoard.Scores = scoreQuery.Take(DefaultScoreCount).ToList<ScoreDTO>();
            return scoreBoard;
        }

    }
}
