﻿using System;
using System.Collections.Generic;

namespace GameAPI.Models
{
    public partial class ScoreBoard
    {
        public ScoreBoard()
        {
            Score = new HashSet<Score>();
        }

        public int Id { get; set; }
        public string ScoreBoardKey { get; set; }
        public string ScoreBoardName { get; set; }
        public bool AllowGuest { get; set; }

        public ICollection<Score> Score { get; set; }
    }
}
