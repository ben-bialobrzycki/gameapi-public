﻿using System;
using System.Collections.Generic;

namespace GameAPI.Models
{
    public partial class Score
    {
        public int Id { get; set; }
        public int? ScoreBoardId { get; set; }
        public int? ScorePoints { get; set; }
        public string ScorePlayerName { get; set; }
        public DateTime? CreateDate { get; set; }

        public ScoreBoard ScoreBoard { get; set; }
    }
}
