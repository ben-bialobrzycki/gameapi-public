﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace GameAPI.Models
{
    public partial class GameAPIDBContext : DbContext
    {
        public virtual DbSet<Score> Score { get; set; }
        public virtual DbSet<ScoreBoard> ScoreBoard { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Score>(entity =>
            {
                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.ScorePlayerName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.ScoreBoard)
                    .WithMany(p => p.Score)
                    .HasForeignKey(d => d.ScoreBoardId)
                    .HasConstraintName("FK__Score__ScoreBoar__25869641");
            });

            modelBuilder.Entity<ScoreBoard>(entity =>
            {
                entity.Property(e => e.ScoreBoardKey)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ScoreBoardName)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });
        }
    }
}
