﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GameAPI;
using GameAPI.Models;

namespace GameAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/ScoreBoard")]
    public class ScoreBoardController : Controller
    {
        private readonly GameAPIDBContext _context;

        public ScoreBoardController(GameAPIDBContext context)
        {
            _context = context;
        }

        [HttpOptions]
        public IActionResult GetOptions()
        {
            return Ok();
        }

        [HttpGet("{apiKey}")]
        public IActionResult GetScoreBoard([FromRoute] string apiKey)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var scoreBoard = _context.GetScoreBoardWithTopScores(apiKey); 

            if (scoreBoard == null)
            {
                return BadRequest();
            }

            return Ok(scoreBoard);
        }
    }
}