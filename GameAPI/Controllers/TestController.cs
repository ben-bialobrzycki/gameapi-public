﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GameAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Test")]
    public class TestController : Controller
    {

        [HttpGet]
        public IActionResult GetTest()
        {
            List<string> values = new List<string>();
            values.Add("Hello");
            values.Add("World");
            return Ok(values);
        }
    }
}