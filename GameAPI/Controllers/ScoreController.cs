﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using GameAPI.Models;
using GameAPI.ModelsDTO;

namespace GameAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Score")]
    public class ScoreController : Controller
    {
        private readonly GameAPIDBContext _context;

        public ScoreController(GameAPIDBContext context)
        {
            _context = context;
        }

        // POST: api/ScoreBoard
        [HttpOptions]
        public IActionResult GetOptions()
        {
            return Ok();
        }
        // POST: api/ScoreBoard
        [HttpPost]
        public  IActionResult PostScoreBoard([FromBody] ScoreDTO score)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }


                if (_context.TryAddScore(score))
                    return new OkResult();
                else
                    return BadRequest();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
        }

    }
}