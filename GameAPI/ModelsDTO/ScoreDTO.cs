﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameAPI.ModelsDTO
{
    public class ScoreDTO
    {
        public int Id { get; set; }
        public string ScoreBoardAPIKey { get; set; }
        public int ScorePoints { get; set; }
        public string ScorePlayerName { get; set; }
        public DateTime CreateDate { get; set; }
    }
}

