﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameAPI.ModelsDTO
{
    public class ScoreBoardDTO
    {
        public int ScoreBoardId { get; set; }
        public string ScoreBoardName { get; set; }
        public List<ScoreDTO> Scores { get; set; }
    }
}
